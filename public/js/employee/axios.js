const createEmployeeRequest = (payload, endpoint) => {
    return new Promise((resolve, reject) => {
        axios.post(endpoint, payload)
            .then((response) => resolve(response))
            .catch((errors) => reject(errors.response.data.errors));
    })
}

const deleteEmployeeRequest = (payload, endpoint) => {
    return new Promise((resolve, reject) => {
        axios.post(endpoint, payload)
            .then((response) => resolve(response))
            .catch((errors) => reject(errors.response));
    })
}

const updateEmployeeRequest = (payload, endpoint) => {
    return new Promise((resolve, reject) => {
        axios.patch(endpoint, payload)
            .then((response) => resolve(response))
            .catch((errors) => reject(errors.response.data.errors));
    })
}