<?php

namespace App\Controller;

use App\Entity\Employee;
use App\Form\EmployeeType;
use App\Repository\EmployeeRepository;
use App\Services\PaginationService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/employees')]
class EmployeeController extends AbstractController
{
    #[Route('/', name: 'app_employee_index', methods: ['GET'])]
    public function index(
        EmployeeRepository $employeeRepository,
        PaginationService $paginationService
    ): Response {
        $query = $employeeRepository->query();
        $paginator = $paginationService->paginate($query);

        return $this->render('employee/index.html.twig', [
            'employees' => $paginator->getItems(),
            'paginator' => $paginator,
            'form' => $this->createForm(EmployeeType::class)->createView(),
        ]);
    }

    #[Route('/new', name: 'app_employee_create', methods: ['GET', 'POST'])]
    public function new(Request $request, EmployeeRepository $employeeRepository): Response
    {
        $employee = new Employee();
        $form = $this->createForm(EmployeeType::class, $employee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $employeeRepository->save($employee, true);

            $this->addFlash('success', "New employee <strong>". $employee->getName() ."</strong> successfully added!");

            return new JsonResponse(['employee' => $employee], Response::HTTP_CREATED);
        }

        return new JsonResponse([ 'errors' => $this->getErrorsFromForm($form)], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    #[Route('/{id}', name: 'app_employee_show', methods: ['GET'])]
    public function show(Employee $employee): Response
    {

    }

    #[Route('/{id}/edit', name: 'app_employee_edit', methods: ['PATCH', 'PUT'])]
    public function edit(Request $request, Employee $employee, EmployeeRepository $employeeRepository): Response
    {
        $form = $this->createForm(EmployeeType::class, $employee, ['method' => $request->getMethod()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $employeeRepository->save($employee, true);

            $this->addFlash('success', "Employee <strong>". $employee->getName() ."</strong> successfully updated!");

            return $this->json(['employee' => $employee], Response::HTTP_CREATED);
        }

        return $this->json([
            'errors' => $this->getErrorsFromForm($form)
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    #[Route('/{id}', name: 'app_employee_delete', methods: ['POST'])]
    public function delete(Request $request, Employee $employee, EmployeeRepository $employeeRepository): Response
    {
        try {
            if ($this->isCsrfTokenValid('delete-employee', $request->request->get('_token'))) {
                $employeeRepository->remove($employee, true);

                $this->addFlash('success', "Employee <strong>". $employee->getName() ."</strong> successfully deleted!");
            } else {
                $this->addFlash('error', "Oops, something went wrong");
            }
        } catch (Exception $exception) {
            $this->addFlash('error', $exception->getMessage());
        }

        return $this->json([], Response::HTTP_OK);
    }

    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }
}
