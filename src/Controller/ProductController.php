<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Requests\Product\ProductCreateRequest;
use App\Requests\Product\ProductUpdateRequest;
use App\Resources\Product\ProductResource;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends AbstractController
{
    public function __construct(
        private ProductResource $productResource,
        private ProductRepository $productRepository
    ) {
    }

    public function index(): JsonResponse {
        $products = $this->productRepository->findAll();

        return $this->productResource->collection($products);
    }

    public function show(Product $product)
    {
        return $this->productResource->item($product);
    }

    public function create(ProductCreateRequest $productCreateRequest): JsonResponse
    {
        $product = Product::create($productCreateRequest->getRequestBody());

        $this->productRepository->save($product, true);

        return $this->productResource->item($product, Response::HTTP_CREATED);
    }

    public function update(Product $product, ProductUpdateRequest $productUpdateRequest): JsonResponse
    {
        $product->update($productUpdateRequest->getRequestBody());

        $this->productRepository->update();

        return $this->productResource->item($product);
    }

    public function delete(Product $product): JsonResponse
    {
        $this->productRepository->remove($product);

        return new JsonResponse([]);
    }
}
