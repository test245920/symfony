<?php

namespace App\Requests\Product;

use App\Entity\Product;
use App\Requests\FormRequest;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ProductCreateRequest extends FormRequest
{
    protected string $entity = Product::class;

    protected function constraints(): Assert\Collection
    {
        return new Assert\Collection([
            'name' => [
                new NotBlank(),
                new Assert\Type(type: 'string'),
                new Assert\Length(min: 5, max: 255),
                new Assert\Callback([
                    'callback' => function ($value, ExecutionContextInterface $context) {
                        if ($this->repository()->findOneBy(['name' => $value])) {
                            $context
                                ->buildViolation('The name must be unique.')
                                ->addViolation();
                        }
                    }
                ]),
            ],
            'price' => [
                new NotBlank(),
                new Assert\Type(type: 'float'),
                new Assert\Range(min: 0, max: 999999)
            ],
            'description' => [
                new NotBlank(),
                new Assert\Type(type: 'string'),
                new Assert\Length(min: 10)
            ],
        ]);
    }
}