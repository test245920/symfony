<?php

namespace App\Requests\Product;

use App\Entity\Product;
use App\Requests\FormRequest;
use Symfony\Component\Validator\Constraints as Assert;

class ProductUpdateRequest extends FormRequest
{
    protected string $entity = Product::class;

    protected function constraints(): Assert\Collection
    {
        return new Assert\Collection([

        ]);
    }
}