<?php

namespace App\Requests;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class FormRequest
{
    protected const ERROR_MESSAGE = 'The given data is invalid';
    protected const ERROR_STATUS = 422;
    protected string $entity;
    private ConstraintViolationList $errors;
    private array $errorsWithMessages = [];
    private Request $currentRequest;

    protected bool $autoValidateRequest = true;

    public function __construct(
        protected ValidatorInterface $validator,
        protected EntityManagerInterface $entityManager,
        protected RequestStack $requestStack
    ) {
        $this->currentRequest = $this->requestStack->getCurrentRequest();
        $this->validate();
    }

    abstract protected function constraints();

    protected function repository(): EntityRepository
    {
        return $this->entityManager->getRepository($this->entity);
    }

    public function validate(string $errorMessage = null, int $errorStatus = null): ?JsonResponse
    {
        $this->errors = $this->validator->validate($this->getRequestBody(), $this->constraints());

        $count = $this->errors->count();
        if ($count > 0) {
            foreach ($this->errors as $error) {
                if ($property = getPropertyName($error)) {
                    $this->errorsWithMessages[$property][] = $error->getMessage();
                }
            }

            if ($this->isApiRequest() && $this->autoValidateRequest) {
                $jsonResponse = new JsonResponse([
                    'message' => $errorMessage ?: static::ERROR_MESSAGE,
                    'errors' => $this->errorsWithMessages
                ], $errorStatus ?: static::ERROR_STATUS);

                return $jsonResponse->send();
            }
        }

        return null;
    }

    public function getRequestBody(): array
    {
        $contentType = $this->currentRequest->headers->get('Content-Type');
        return ($contentType === 'application/json')
            ? json_decode($this->currentRequest->getContent(), true)
            : $this->currentRequest->request->all();
    }

    public function getErrors(): ConstraintViolationList
    {
        return $this->errors;
    }

    public function getErrorsMessages(): array
    {
        return $this->errorsWithMessages;
    }

    public function isApiRequest(): bool
    {
        return startsWith($this->currentRequest->getPathInfo(), '/api/');
    }
}

function startsWith(string $haystack, string $needle): bool
{
    return substr($haystack, 0, strlen($needle)) === $needle;
}

function getPropertyName(ConstraintViolation $constraintViolation): ?string
{
    if (preg_match('/\[(.*?)\]/', $constraintViolation->getPropertyPath(), $matches)) {
        return $matches[1];
    }

    return null;
}
