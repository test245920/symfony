<?php

namespace App\Resources\Product;

use App\Resources\EntityResource;

class ProductResource extends EntityResource
{
    public function normalizeEntity($entity): array
    {
        return parent::normalize($entity);
    }

    protected function additional(): array
    {
        return [
            'meta' => [
                'page' => 2
            ]
        ];
    }
}