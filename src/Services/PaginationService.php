<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

final class PaginationService
{
    public const DEFAULT_PAGE = 1;
    public const DEFAULT_PER_PAGE = 4;

    private ?Request $request;
    private int|null|float $total;
    private int $lastPage;
    private Paginator $items;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function paginate(Query $query): self
    {
        $paginator = new Paginator($query, true);

        $page = $this->request->query->getInt('page', self::DEFAULT_PAGE);
        $limit = $this->request->query->getInt('perPage', self::DEFAULT_PER_PAGE);

        $paginator
            ->getQuery()
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit);

        $this->total = $paginator->count();
        $this->lastPage = (int) ceil($paginator->count() / $paginator->getQuery()->getMaxResults());
        $this->items = $paginator;

        return $this;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function getLastPage(): int
    {
        return $this->lastPage;
    }

    public function getItems()
    {
        return $this->items;
    }
}