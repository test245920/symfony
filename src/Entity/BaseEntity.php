<?php

namespace App\Entity;

abstract class BaseEntity
{
    public static function create(array $data): static
    {
        $entity = new static();

        foreach ($data as $property => $value) {
            $setterMethodName = 'set' . ucfirst($property);
            if (method_exists($entity, $setterMethodName)) {
                $entity->$setterMethodName($value);
            }
        }

        return $entity;
    }

    public function update(array $data): static
    {
        foreach ($data as $property => $value) {
            $setterMethodName = 'set' . ucfirst($property);
            if (method_exists($this, $setterMethodName)) {
                $this->$setterMethodName($value);
            }
        }

        return $this;
    }

    public function toArray(): array
    {
        $reflectionClass = new \ReflectionClass($this);
        $properties = $reflectionClass->getProperties();

        foreach ($properties as $property) {
            $propertyName = $property->getName();
            $getter = 'get' . ucfirst($propertyName);

            if (method_exists($this, $getter)) {
                $value = $this->$getter();
                $data[$propertyName] = $value;
            }
        }

        return $data;
    }
}