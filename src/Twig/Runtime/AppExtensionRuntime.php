<?php

namespace App\Twig\Runtime;

use Twig\Extension\RuntimeExtensionInterface;

class AppExtensionRuntime implements RuntimeExtensionInterface
{
    public function __construct()
    {
        // Inject dependencies if needed
    }

    public function doSomething($value)
    {
        // ...
    }

    public function makePriceFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ','): string
    {
        return '$' . number_format($number, $decimals, $decPoint, $thousandsSep);
    }

    public function makeCalculateFunction(int $with, int $height)
    {
        return $with * $height;
    }
}
