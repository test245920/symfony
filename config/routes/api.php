<?php

use App\Controller\ProductController;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $routes) {
    $routes
        ->add('products_index', '/api/products')
        ->controller([ProductController::class, 'index'])
        ->methods(['GET', 'HEAD'])
        ->requirements(['product' => '\d+']);

    $routes
        ->add('products_create', '/api/products')
        ->controller([ProductController::class, 'create'])
        ->methods(['POST']);

    $routes
        ->add('products_show', '/api/products/{product}')
        ->controller([ProductController::class, 'show'])
        ->methods(['GET'])
        ->requirements(['product' => '\d+']);

    $routes
        ->add('products_update', '/api/products/{product}')
        ->controller([ProductController::class, 'update'])
        ->methods(['PUT', 'PATCH'])
        ->requirements(['product' => '\d+']);

    $routes
        ->add('products_delete', '/api/products/{product}')
        ->controller([ProductController::class, 'delete'])
        ->methods(['DELETE'])
        ->requirements(['product' => '\d+']);
};